import { readData, readDatas } from '../utils/readDatas';
import { readDir } from '../utils/shared';

export default class DataPage {
    public pathProject: string;
    public pathFiles: string;

    constructor(pathProject: string, pathFiles: string) {
        this.pathProject = pathProject;
        this.pathFiles = pathFiles;
    }

    get readData(): any[] {
        const rPage = readData(this.pathProject);
        const readFiles = readDir(this.pathProject);

        return readDatas(rPage)(readFiles)(
            readFiles(this.pathFiles),
            this.pathFiles,
        )({});
    }
}
