import path from 'path';
import { IPage } from '../index';
import {
    readFilesCustom,
    readFilesCustomFlat,
    readPage,
} from '../utils/readPages';
import { readDir } from '../utils/shared';

export default class CmsPages {
    public pathProject: string;
    public pathFiles: string;
    public pathFull: string;

    public options: {
        root: string;
    };

    constructor(
        pathProject: string,
        pathFiles: string,
        options = { root: '' },
    ) {
        this.pathProject = pathProject;
        this.pathFiles = pathFiles;
        this.options = options;
        this.pathFull = path.resolve(pathProject, pathFiles);
    }

    get readPagesTree(): any[] {
        const rPage = readPage(
            this.pathProject,
            this.pathFiles,
            this.options.root,
        );
        return readFilesCustom(rPage)(readDir(this.pathFull))(
            this.readFilesFolder,
        );
    }
    get readPagesList(): IPage[] {
        const rPage = readPage(
            this.pathProject,
            this.pathFiles,
            this.options.root,
        );
        return readFilesCustomFlat(rPage)(readDir(this.pathFull))(
            this.readFilesFolder,
        )([]);
    }

    get readFilesFolder(): string[] {
        return readDir(this.pathFull)();
    }
}
