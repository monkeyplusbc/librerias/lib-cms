import fs from 'fs';
import path from 'path';
import DataClass from './classes/Data';
import PagesClass from './classes/Pages';
import { IContext, IContextGlobal, IPage, IPathsFiles, ISite } from './index';
import { readData, uuid } from './utils/readDatas';
import { readDir, readImages } from './utils/shared';

const Console = console;
export function BuildPages(pathProject: string, pathFiles: string): PagesClass {
    return new PagesClass(pathProject, pathFiles);
}

export function BuildData(pathProject: string, pathFiles: string): DataClass {
    return new DataClass(pathProject, pathFiles);
}

export const MyPage = (pathProject: string, paths: IPathsFiles) => (
    url: string,
) => {
    const Routes = new PagesClass(pathProject, paths.pathRoutes);
    const Blogs = new PagesClass(pathProject, paths.pathBlogs, {
        root: 'blogs',
    });

    let AllPages: IPage[] = [];
    AllPages = AllPages.concat(Routes.readPagesList, Blogs.readPagesList);

    const page = AllPages.find(p => p.fullUrl === url);

    if (page) {
        const LINKS = AllPages.map(p => {
            return {
                id: p.id,
                url: p.fullUrl,
            };
        });
        const GLOBALS = new DataClass(pathProject, paths.pathGlobals).readData;
        let myContext: IContext = {
            PAGE: page,
            LINKS,
            GLOBALS,
            SEO: page.seo,
        };
        if (page.bundle) {
            myContext.BUNDLE = page.bundle;
        }
        if (page.data) {
            if (!page.data.includes('.json')) {
                const Datas = new DataClass(pathProject, page.data);
                const context = Datas.readData;
                myContext = { ...myContext, ...context };
            } else {
                const d = readData(pathProject)(page.data);
                const context = uuid(readData(pathProject))(d)({});
                myContext = { ...myContext, ...context };
            }
        }
        return myContext;
    } else {
        return undefined;
    }
};

export const buildSite = (
    relativeTo: string,
    paths: { pathRoutes: string; pathGlobals: string },
    otherRoutes: any[] = [],
): ISite => {
    function getCountext(): IContextGlobal {
        const Routes = new PagesClass(relativeTo, paths.pathRoutes);
        const OtherRoutes = otherRoutes.map(doc => {
            return new PagesClass(relativeTo, doc.pathFolder, {
                root: doc.root,
            }).readPagesList;
        });
        let AllPages: IPage[] = [];

        AllPages = AllPages.concat(Routes.readPagesList, ...OtherRoutes);
        const LINKS: any = {};
        AllPages.forEach(p => {
            LINKS[p.id!] = p.fullUrl;
            return {
                id: p.id,
                url: p.fullUrl,
            };
        });
        const GLOBALS = new DataClass(relativeTo, paths.pathGlobals).readData;
        return {
            LINKS,
            GLOBALS,
            PAGES: AllPages,
            ROUTES: {
                go: (id: string): string => {
                    if (LINKS[id]) {
                        return LINKS[id];
                    } else {
                        return '404.html';
                    }
                },
            },
        };
    }

    function getPage(url: string) {
        const context = getCountext();
        const page = context.PAGES.find(p => p.fullUrl === url);
        if (page) {
            const endContext: any = {
                local: {},
                global: {},
            };
            const myContext: IContext = {
                PAGE: page,
                LINKS: context.LINKS,
                GLOBALS: context.GLOBALS,
                SEO: page.seo,
                ROUTES: {
                    go: (id: string): string => {
                        if (context.LINKS[id]) {
                            return context.LINKS[id];
                        } else {
                            return '404.html';
                        }
                    },
                },
            };
            if (page.bundle) {
                myContext.BUNDLE = page.bundle;
            }
            endContext.global = myContext;
            if (page.data) {
                if (!page.data.includes('.json')) {
                    const Datas = new DataClass(relativeTo, page.data);
                    const lContext = Datas.readData;
                    endContext.local = lContext;
                } else {
                    const d = readData(relativeTo)(page.data);
                    const lContext = uuid(readData(relativeTo))(d)({});
                    endContext.local = lContext;
                }
            }
            return endContext;
        } else {
            return undefined;
        }
    }

    return {
        context: getCountext,
        page: getPage,
    };
};

export const generateFolders = (pathProject: string) => (dirs: {
    origin: string;
    output: string;
}) => {
    const pages = readDir(pathProject)(dirs.origin);
    pages.forEach(page => {
        if (!page.includes('.json')) {
            const pathEnd = path.join(pathProject, dirs.output, page);
            fs.mkdirSync(pathEnd);
            Console.log('Crear carpeta', pathEnd);
            generateFolders(path.join(pathProject))({
                origin: path.join(dirs.origin, page),
                output: path.join(dirs.output, page),
            });
        }
    });
};

export const BuildImages = (pathProject: string) => (
    pathFiles: string,
    pathUrl: string,
): any => {
    const files = readDir(pathProject)(pathFiles);
    const all = readImages(files, path.join(pathProject, pathFiles), pathUrl);
    return all;
};
