import PagesClass from './classes/Pages';
import {
    BuildData,
    BuildImages,
    BuildPages,
    generateFolders,
    MyPage,
    buildSite,
} from './libs';
import * as ReadDatas from './utils/readDatas';

/**
 * Librerias
 */
export const FilterLinx = ReadDatas.filterLinx;

export const Pages = BuildPages;
export const Data = BuildData;
export const Page = MyPage;
export const Site = buildSite;

export const makeFolders = generateFolders;

export const readImages = BuildImages;
export const ReadPages = PagesClass;

export function addParams(validParams: string[][], body: any) {
    const params: any = {};
    for (const [key, value] of Object.entries(body)) {
        validParams.forEach(v => {
            if (v.includes(key)) {
                params[v[0]] = value;
            }
        });
    }
    return params;
}

/**
 * Tipos Globlales
 */
export type TReadFile = (name: string, folder: string) => IPage | {};
export type TReadFilesFolder = (folder: string) => string[];
export type TReadFilesDir = (folder: string) => (folder: string) => string[];

export interface IPage {
    url: string;
    path?: string;
    id?: string;
    seo: any;
    data?: string;
    img?: string;
    bundle?: string;

    fullUrl?: string;
}
export interface ISite {
    context: () => IContextGlobal;
    page: (url: string) => IContext | undefined;
}
export enum TypesLinx {
    All = 'all',
    Filter = 'filter',
    Find = 'find',
}
interface IFilterLinx {
    key: string;
    value: string[] | boolean | string;
}
export interface ILinx {
    path: string;
    name: string;
    type: string;
    value?: IFilterLinx[] | string;
}

export interface IContext {
    PAGE: IPage;
    SEO?: any;
    LINKS: any;
    GLOBALS: any;
    BUNDLE?: string;
    ROUTES?: any;
}

export interface IContextGlobal {
    LINKS: any;
    GLOBALS: any;
    PAGES: IPage[];
    ROUTES: any;
}

export interface IPathsFiles {
    pathRoutes: string;
    pathBlogs: string;
    pathGlobals: string;
}
