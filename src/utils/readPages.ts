import fs from 'fs';
import path from 'path';
import { IPage, TReadFilesFolder, TReadFile } from '../index';

export const readFilesCustom = (readSinglePage: TReadFile) => (
    readFilesFolder: TReadFilesFolder,
) => (files: string[], folder: string = ''): any[] => {
    return files.map(nameFile => {
        if (nameFile.includes('.json')) {
            return readSinglePage(nameFile, folder);
        } else {
            const pathB = path.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            const children = readFilesCustom(readSinglePage)(readFilesFolder)(
                a,
                pathB,
            );
            return {
                id: pathB,
                children,
            };
        }
    });
};
export const readFilesCustomFlat = (readSinglePage: TReadFile) => (
    readFilesFolder: TReadFilesFolder,
) => (files: string[], folder: string = '') => (list: any[]) => {
    files.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            list.push(readSinglePage(nameFile, folder));
        } else {
            const pathB = path.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            list = list.concat(
                readFilesCustomFlat(readSinglePage)(readFilesFolder)(a, pathB)(
                    [],
                ),
            );
        }
    });
    return list;
};

export const readPage = (
    pathProject: string,
    pathFiles: string,
    root: string = '',
) => (name: string, folder: string = ''): IPage | {} => {
    const uuid: string = path.join(folder, name);

    const pathFile = path.join(pathProject, pathFiles, folder, name);

    try {
        const file = fs.readFileSync(pathFile, 'utf8');
        const fileParse: IPage = JSON.parse(file);
        if (fileParse.url) {
            fileParse.fullUrl = path.join(root, folder, fileParse.url);
        }
        const obj = {
            path: path.join(pathFiles, uuid),
            id: path.join(root, uuid.replace('.json', '.html')),
            ...fileParse,
        };
        return obj;
    } catch (error) {
        // Lanzar error critico de no existencia
        return {};
    }
};
