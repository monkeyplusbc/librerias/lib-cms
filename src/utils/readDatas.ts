import fs from 'fs';

import path from 'path';
import { ILinx, TReadFile, TReadFilesFolder, TypesLinx } from '../index';

export const readDatas = (fReadSingleFile: TReadFile) => (
    readFilesFolder: TReadFilesFolder,
) => (files: string[], folder: string = '') => (obj: any) => {
    files.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            const data = fReadSingleFile(nameFile, folder);
            const d = uuid(fReadSingleFile)(data, path.join(folder, nameFile))(
                {},
            );
            obj[nameFile.replace('.json', '')] = d;
        } else {
            const pathB = path.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            obj[nameFile] = readDatas(fReadSingleFile)(readFilesFolder)(
                a,
                pathB,
            )({});
        }
    });
    return obj;
};

export const readData = (pathProject: string) => (
    name: string,
    folder: string = '',
) => {
    const pathFile = path.join(pathProject, folder, name);
    try {
        const file = fs.readFileSync(pathFile, 'utf8');
        const obj = JSON.parse(file);
        return obj;
    } catch (error) {
        // Lanzar error critico de no existencia
        return {};
    }
};

export const uuid = (readFiles: TReadFile) => (
    obj: { [key: string]: any },
    folder: string = '',
) => (nObj: any = {}) => {
    for (const [key, value] of Object.entries(obj)) {
        const UUID = path.join(folder, key);

        if (value.hasOwnProperty('value')) {
            nObj[key] = { $uuid: UUID, ...value };
        } else if (value.hasOwnProperty('linx')) {
            const d = readFiles(value.linx.name, value.linx.path);
            nObj[key] = filterLinx(
                uuid(readFiles)(
                    d,
                    `${key}@${value.linx.type}:` +
                        path.join(value.linx.path, value.linx.name as string),
                )({}),
                value.linx,
            );
        } else if (typeof value === 'string' || typeof value === 'boolean') {
            nObj[key] = value;
        } else if (Array.isArray(value)) {
            nObj[key] = value;
        } else {
            nObj[key] = uuid(readFiles)(value, UUID)({});
        }
    }
    return nObj;
};

enum Response {
    No = 0,
    Yes = 1,
}

export const filterLinx = (data: { [key: string]: any }, linx: ILinx) => {
    if (linx.type === TypesLinx.All) {
        return data;
    } else if (linx.type === TypesLinx.Find) {
        return data[linx.value as string];
    } else if (linx.type === TypesLinx.Filter) {
        const f: any = {};
        if (typeof linx.value === 'string') {
            for (const [key, value] of Object.entries(data)) {
                if (value[linx.value]) {
                    f[key] = value;
                }
            }
        } else {
            for (const [key, value] of Object.entries(data)) {
                const pass = linx
                    .value!.map(v => {
                        if (typeof v.value === 'boolean') {
                            return compareValsBool(value[v.key], v.value);
                        }
                        if (typeof v.value === 'string') {
                            return compareValsString(value[v.key], v.value);
                        } else {
                            return compareValsStrings(value[v.key], v.value);
                        }
                    })
                    .every(o => o === true);

                if (pass) {
                    f[key] = value;
                }
            }
        }
        return f;
    }
};

function compareValsBool(
    data: undefined | boolean | string | { [key: string]: any },
    val: boolean,
) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return data === val;
    }
    if (typeof data === 'string') {
        return true;
    } else {
        return data.value === val;
    }
}

function compareValsString(
    data: boolean | string | { [key: string]: any },
    val: string,
) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return false;
    }
    if (typeof data === 'string') {
        return data === val;
    } else {
        if (Array.isArray(data)) {
            const a = data.indexOf(val);
            if (a > -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return data.value === val;
        }
    }
}

function compareValsStrings(
    data: boolean | string | { [key: string]: any },
    val: string[],
) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return false;
    }
    if (typeof data === 'string') {
        const a = val.indexOf(data);
        if (a > -1) {
            return true;
        } else {
            return false;
        }
    } else {
        if (Array.isArray(data)) {
            return data.some(r => val.includes(r));
        } else {
            const a = val.indexOf(data.value);
            if (a > -1) {
                return true;
            } else {
                return false;
            }
        }
    }
}
