import fs from 'fs';

import path from 'path';
export const readDir = (pathFolder: string) => (
    folder: string = '',
): string[] => {
    try {
        const files = fs.readdirSync(path.join(pathFolder, folder), 'utf8');
        return files;
    } catch (error) {
        console.error(`La carpeta ${path.join(pathFolder, folder)}, no existe`);
        return [];
    }
};

export function readImages(
    list: string[],
    pathFiles: string,
    pathUrl: string,
): any {
    const obj: any = {};
    list.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            const p = path.join(pathFiles, nameFile);
            const file = fs.readFileSync(p, 'utf8');
            const fileParse = JSON.parse(file);
            for (const key in fileParse) {
                const pp = path.join(nameFile.replace('.json', ''), key);
                const src = path.join(
                    pathUrl,
                    pp
                        .split('/')
                        .reverse()
                        .slice(1)
                        .reverse()
                        .join('/'),
                    `${fileParse[key].name}.${key.split('.')[1]}`,
                );
                obj[pp] = { ...fileParse[key], src };
            }
        }
    });

    return obj;
}
