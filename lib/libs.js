"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const Data_1 = __importDefault(require("./classes/Data"));
const Pages_1 = __importDefault(require("./classes/Pages"));
const readDatas_1 = require("./utils/readDatas");
const shared_1 = require("./utils/shared");
const Console = console;
function BuildPages(pathProject, pathFiles) {
    return new Pages_1.default(pathProject, pathFiles);
}
exports.BuildPages = BuildPages;
function BuildData(pathProject, pathFiles) {
    return new Data_1.default(pathProject, pathFiles);
}
exports.BuildData = BuildData;
exports.MyPage = (pathProject, paths) => (url) => {
    const Routes = new Pages_1.default(pathProject, paths.pathRoutes);
    const Blogs = new Pages_1.default(pathProject, paths.pathBlogs, {
        root: 'blogs',
    });
    let AllPages = [];
    AllPages = AllPages.concat(Routes.readPagesList, Blogs.readPagesList);
    const page = AllPages.find(p => p.fullUrl === url);
    if (page) {
        const LINKS = AllPages.map(p => {
            return {
                id: p.id,
                url: p.fullUrl,
            };
        });
        const GLOBALS = new Data_1.default(pathProject, paths.pathGlobals).readData;
        let myContext = {
            PAGE: page,
            LINKS,
            GLOBALS,
            SEO: page.seo,
        };
        if (page.bundle) {
            myContext.BUNDLE = page.bundle;
        }
        if (page.data) {
            if (!page.data.includes('.json')) {
                const Datas = new Data_1.default(pathProject, page.data);
                const context = Datas.readData;
                myContext = { ...myContext, ...context };
            }
            else {
                const d = readDatas_1.readData(pathProject)(page.data);
                const context = readDatas_1.uuid(readDatas_1.readData(pathProject))(d)({});
                myContext = { ...myContext, ...context };
            }
        }
        return myContext;
    }
    else {
        return undefined;
    }
};
exports.buildSite = (relativeTo, paths, otherRoutes = []) => {
    function getCountext() {
        const Routes = new Pages_1.default(relativeTo, paths.pathRoutes);
        const OtherRoutes = otherRoutes.map(doc => {
            return new Pages_1.default(relativeTo, doc.pathFolder, {
                root: doc.root,
            }).readPagesList;
        });
        let AllPages = [];
        AllPages = AllPages.concat(Routes.readPagesList, ...OtherRoutes);
        const LINKS = {};
        AllPages.forEach(p => {
            LINKS[p.id] = p.fullUrl;
            return {
                id: p.id,
                url: p.fullUrl,
            };
        });
        const GLOBALS = new Data_1.default(relativeTo, paths.pathGlobals).readData;
        return {
            LINKS,
            GLOBALS,
            PAGES: AllPages,
            ROUTES: {
                go: (id) => {
                    if (LINKS[id]) {
                        return LINKS[id];
                    }
                    else {
                        return '404.html';
                    }
                },
            },
        };
    }
    function getPage(url) {
        const context = getCountext();
        const page = context.PAGES.find(p => p.fullUrl === url);
        if (page) {
            const endContext = {
                local: {},
                global: {},
            };
            const myContext = {
                PAGE: page,
                LINKS: context.LINKS,
                GLOBALS: context.GLOBALS,
                SEO: page.seo,
                ROUTES: {
                    go: (id) => {
                        if (context.LINKS[id]) {
                            return context.LINKS[id];
                        }
                        else {
                            return '404.html';
                        }
                    },
                },
            };
            if (page.bundle) {
                myContext.BUNDLE = page.bundle;
            }
            endContext.global = myContext;
            if (page.data) {
                if (!page.data.includes('.json')) {
                    const Datas = new Data_1.default(relativeTo, page.data);
                    const lContext = Datas.readData;
                    endContext.local = lContext;
                }
                else {
                    const d = readDatas_1.readData(relativeTo)(page.data);
                    const lContext = readDatas_1.uuid(readDatas_1.readData(relativeTo))(d)({});
                    endContext.local = lContext;
                }
            }
            return endContext;
        }
        else {
            return undefined;
        }
    }
    return {
        context: getCountext,
        page: getPage,
    };
};
exports.generateFolders = (pathProject) => (dirs) => {
    const pages = shared_1.readDir(pathProject)(dirs.origin);
    pages.forEach(page => {
        if (!page.includes('.json')) {
            const pathEnd = path_1.default.join(pathProject, dirs.output, page);
            fs_1.default.mkdirSync(pathEnd);
            Console.log('Crear carpeta', pathEnd);
            exports.generateFolders(path_1.default.join(pathProject))({
                origin: path_1.default.join(dirs.origin, page),
                output: path_1.default.join(dirs.output, page),
            });
        }
    });
};
exports.BuildImages = (pathProject) => (pathFiles, pathUrl) => {
    const files = shared_1.readDir(pathProject)(pathFiles);
    const all = shared_1.readImages(files, path_1.default.join(pathProject, pathFiles), pathUrl);
    return all;
};
//# sourceMappingURL=libs.js.map