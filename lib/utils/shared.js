"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
exports.readDir = (pathFolder) => (folder = '') => {
    try {
        const files = fs_1.default.readdirSync(path_1.default.join(pathFolder, folder), 'utf8');
        return files;
    }
    catch (error) {
        console.error(`La carpeta ${path_1.default.join(pathFolder, folder)}, no existe`);
        return [];
    }
};
function readImages(list, pathFiles, pathUrl) {
    const obj = {};
    list.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            const p = path_1.default.join(pathFiles, nameFile);
            const file = fs_1.default.readFileSync(p, 'utf8');
            const fileParse = JSON.parse(file);
            for (const key in fileParse) {
                const pp = path_1.default.join(nameFile.replace('.json', ''), key);
                const src = path_1.default.join(pathUrl, pp
                    .split('/')
                    .reverse()
                    .slice(1)
                    .reverse()
                    .join('/'), `${fileParse[key].name}.${key.split('.')[1]}`);
                obj[pp] = { ...fileParse[key], src };
            }
        }
    });
    return obj;
}
exports.readImages = readImages;
//# sourceMappingURL=shared.js.map