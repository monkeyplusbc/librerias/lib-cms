import { IPage, TReadFilesFolder, TReadFile } from '../index';
export declare const readFilesCustom: (readSinglePage: TReadFile) => (readFilesFolder: TReadFilesFolder) => (files: string[], folder?: string) => any[];
export declare const readFilesCustomFlat: (readSinglePage: TReadFile) => (readFilesFolder: TReadFilesFolder) => (files: string[], folder?: string) => (list: any[]) => any[];
export declare const readPage: (pathProject: string, pathFiles: string, root?: string) => (name: string, folder?: string) => {} | IPage;
