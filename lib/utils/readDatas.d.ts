import { ILinx, TReadFile, TReadFilesFolder } from '../index';
export declare const readDatas: (fReadSingleFile: TReadFile) => (readFilesFolder: TReadFilesFolder) => (files: string[], folder?: string) => (obj: any) => any;
export declare const readData: (pathProject: string) => (name: string, folder?: string) => any;
export declare const uuid: (readFiles: TReadFile) => (obj: {
    [key: string]: any;
}, folder?: string) => (nObj?: any) => any;
export declare const filterLinx: (data: {
    [key: string]: any;
}, linx: ILinx) => any;
