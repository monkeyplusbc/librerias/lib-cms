"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
exports.readFilesCustom = (readSinglePage) => (readFilesFolder) => (files, folder = '') => {
    return files.map(nameFile => {
        if (nameFile.includes('.json')) {
            return readSinglePage(nameFile, folder);
        }
        else {
            const pathB = path_1.default.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            const children = exports.readFilesCustom(readSinglePage)(readFilesFolder)(a, pathB);
            return {
                id: pathB,
                children,
            };
        }
    });
};
exports.readFilesCustomFlat = (readSinglePage) => (readFilesFolder) => (files, folder = '') => (list) => {
    files.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            list.push(readSinglePage(nameFile, folder));
        }
        else {
            const pathB = path_1.default.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            list = list.concat(exports.readFilesCustomFlat(readSinglePage)(readFilesFolder)(a, pathB)([]));
        }
    });
    return list;
};
exports.readPage = (pathProject, pathFiles, root = '') => (name, folder = '') => {
    const uuid = path_1.default.join(folder, name);
    const pathFile = path_1.default.join(pathProject, pathFiles, folder, name);
    try {
        const file = fs_1.default.readFileSync(pathFile, 'utf8');
        const fileParse = JSON.parse(file);
        if (fileParse.url) {
            fileParse.fullUrl = path_1.default.join(root, folder, fileParse.url);
        }
        const obj = {
            path: path_1.default.join(pathFiles, uuid),
            id: path_1.default.join(root, uuid.replace('.json', '.html')),
            ...fileParse,
        };
        return obj;
    }
    catch (error) {
        return {};
    }
};
//# sourceMappingURL=readPages.js.map