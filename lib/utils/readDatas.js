"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const index_1 = require("../index");
exports.readDatas = (fReadSingleFile) => (readFilesFolder) => (files, folder = '') => (obj) => {
    files.forEach(nameFile => {
        if (nameFile.includes('.json')) {
            const data = fReadSingleFile(nameFile, folder);
            const d = exports.uuid(fReadSingleFile)(data, path_1.default.join(folder, nameFile))({});
            obj[nameFile.replace('.json', '')] = d;
        }
        else {
            const pathB = path_1.default.join(folder, nameFile);
            const a = readFilesFolder(pathB);
            obj[nameFile] = exports.readDatas(fReadSingleFile)(readFilesFolder)(a, pathB)({});
        }
    });
    return obj;
};
exports.readData = (pathProject) => (name, folder = '') => {
    const pathFile = path_1.default.join(pathProject, folder, name);
    try {
        const file = fs_1.default.readFileSync(pathFile, 'utf8');
        const obj = JSON.parse(file);
        return obj;
    }
    catch (error) {
        return {};
    }
};
exports.uuid = (readFiles) => (obj, folder = '') => (nObj = {}) => {
    for (const [key, value] of Object.entries(obj)) {
        const UUID = path_1.default.join(folder, key);
        if (value.hasOwnProperty('value')) {
            nObj[key] = { $uuid: UUID, ...value };
        }
        else if (value.hasOwnProperty('linx')) {
            const d = readFiles(value.linx.name, value.linx.path);
            nObj[key] = exports.filterLinx(exports.uuid(readFiles)(d, `${key}@${value.linx.type}:` +
                path_1.default.join(value.linx.path, value.linx.name))({}), value.linx);
        }
        else if (typeof value === 'string' || typeof value === 'boolean') {
            nObj[key] = value;
        }
        else if (Array.isArray(value)) {
            nObj[key] = value;
        }
        else {
            nObj[key] = exports.uuid(readFiles)(value, UUID)({});
        }
    }
    return nObj;
};
var Response;
(function (Response) {
    Response[Response["No"] = 0] = "No";
    Response[Response["Yes"] = 1] = "Yes";
})(Response || (Response = {}));
exports.filterLinx = (data, linx) => {
    if (linx.type === index_1.TypesLinx.All) {
        return data;
    }
    else if (linx.type === index_1.TypesLinx.Find) {
        return data[linx.value];
    }
    else if (linx.type === index_1.TypesLinx.Filter) {
        const f = {};
        if (typeof linx.value === 'string') {
            for (const [key, value] of Object.entries(data)) {
                if (value[linx.value]) {
                    f[key] = value;
                }
            }
        }
        else {
            for (const [key, value] of Object.entries(data)) {
                const pass = linx
                    .value.map(v => {
                    if (typeof v.value === 'boolean') {
                        return compareValsBool(value[v.key], v.value);
                    }
                    if (typeof v.value === 'string') {
                        return compareValsString(value[v.key], v.value);
                    }
                    else {
                        return compareValsStrings(value[v.key], v.value);
                    }
                })
                    .every(o => o === true);
                if (pass) {
                    f[key] = value;
                }
            }
        }
        return f;
    }
};
function compareValsBool(data, val) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return data === val;
    }
    if (typeof data === 'string') {
        return true;
    }
    else {
        return data.value === val;
    }
}
function compareValsString(data, val) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return false;
    }
    if (typeof data === 'string') {
        return data === val;
    }
    else {
        if (Array.isArray(data)) {
            const a = data.indexOf(val);
            if (a > -1) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return data.value === val;
        }
    }
}
function compareValsStrings(data, val) {
    if (typeof data === 'undefined') {
        return false;
    }
    if (typeof data === 'boolean') {
        return false;
    }
    if (typeof data === 'string') {
        const a = val.indexOf(data);
        if (a > -1) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (Array.isArray(data)) {
            return data.some(r => val.includes(r));
        }
        else {
            const a = val.indexOf(data.value);
            if (a > -1) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
//# sourceMappingURL=readDatas.js.map