import PagesClass from './classes/Pages';
import { BuildData, BuildPages } from './libs';
export declare const FilterLinx: (data: {
    [key: string]: any;
}, linx: ILinx) => any;
export declare const Pages: typeof BuildPages;
export declare const Data: typeof BuildData;
export declare const Page: (pathProject: string, paths: IPathsFiles) => (url: string) => IContext | undefined;
export declare const Site: (relativeTo: string, paths: {
    pathRoutes: string;
    pathGlobals: string;
}, otherRoutes?: any[]) => ISite;
export declare const makeFolders: (pathProject: string) => (dirs: {
    origin: string;
    output: string;
}) => void;
export declare const readImages: (pathProject: string) => (pathFiles: string, pathUrl: string) => any;
export declare const ReadPages: typeof PagesClass;
export declare function addParams(validParams: string[][], body: any): any;
export declare type TReadFile = (name: string, folder: string) => IPage | {};
export declare type TReadFilesFolder = (folder: string) => string[];
export declare type TReadFilesDir = (folder: string) => (folder: string) => string[];
export interface IPage {
    url: string;
    path?: string;
    id?: string;
    seo: any;
    data?: string;
    img?: string;
    bundle?: string;
    fullUrl?: string;
}
export interface ISite {
    context: () => IContextGlobal;
    page: (url: string) => IContext | undefined;
}
export declare enum TypesLinx {
    All = "all",
    Filter = "filter",
    Find = "find"
}
interface IFilterLinx {
    key: string;
    value: string[] | boolean | string;
}
export interface ILinx {
    path: string;
    name: string;
    type: string;
    value?: IFilterLinx[] | string;
}
export interface IContext {
    PAGE: IPage;
    SEO?: any;
    LINKS: any;
    GLOBALS: any;
    BUNDLE?: string;
    ROUTES?: any;
}
export interface IContextGlobal {
    LINKS: any;
    GLOBALS: any;
    PAGES: IPage[];
    ROUTES: any;
}
export interface IPathsFiles {
    pathRoutes: string;
    pathBlogs: string;
    pathGlobals: string;
}
export {};
