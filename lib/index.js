"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Pages_1 = __importDefault(require("./classes/Pages"));
const libs_1 = require("./libs");
const ReadDatas = __importStar(require("./utils/readDatas"));
exports.FilterLinx = ReadDatas.filterLinx;
exports.Pages = libs_1.BuildPages;
exports.Data = libs_1.BuildData;
exports.Page = libs_1.MyPage;
exports.Site = libs_1.buildSite;
exports.makeFolders = libs_1.generateFolders;
exports.readImages = libs_1.BuildImages;
exports.ReadPages = Pages_1.default;
function addParams(validParams, body) {
    const params = {};
    for (const [key, value] of Object.entries(body)) {
        validParams.forEach(v => {
            if (v.includes(key)) {
                params[v[0]] = value;
            }
        });
    }
    return params;
}
exports.addParams = addParams;
var TypesLinx;
(function (TypesLinx) {
    TypesLinx["All"] = "all";
    TypesLinx["Filter"] = "filter";
    TypesLinx["Find"] = "find";
})(TypesLinx = exports.TypesLinx || (exports.TypesLinx = {}));
//# sourceMappingURL=index.js.map