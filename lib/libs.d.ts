import DataClass from './classes/Data';
import PagesClass from './classes/Pages';
import { IContext, IPathsFiles, ISite } from './index';
export declare function BuildPages(pathProject: string, pathFiles: string): PagesClass;
export declare function BuildData(pathProject: string, pathFiles: string): DataClass;
export declare const MyPage: (pathProject: string, paths: IPathsFiles) => (url: string) => IContext | undefined;
export declare const buildSite: (relativeTo: string, paths: {
    pathRoutes: string;
    pathGlobals: string;
}, otherRoutes?: any[]) => ISite;
export declare const generateFolders: (pathProject: string) => (dirs: {
    origin: string;
    output: string;
}) => void;
export declare const BuildImages: (pathProject: string) => (pathFiles: string, pathUrl: string) => any;
