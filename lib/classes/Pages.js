"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const readPages_1 = require("../utils/readPages");
const shared_1 = require("../utils/shared");
class CmsPages {
    constructor(pathProject, pathFiles, options = { root: '' }) {
        this.pathProject = pathProject;
        this.pathFiles = pathFiles;
        this.options = options;
        this.pathFull = path_1.default.resolve(pathProject, pathFiles);
    }
    get readPagesTree() {
        const rPage = readPages_1.readPage(this.pathProject, this.pathFiles, this.options.root);
        return readPages_1.readFilesCustom(rPage)(shared_1.readDir(this.pathFull))(this.readFilesFolder);
    }
    get readPagesList() {
        const rPage = readPages_1.readPage(this.pathProject, this.pathFiles, this.options.root);
        return readPages_1.readFilesCustomFlat(rPage)(shared_1.readDir(this.pathFull))(this.readFilesFolder)([]);
    }
    get readFilesFolder() {
        return shared_1.readDir(this.pathFull)();
    }
}
exports.default = CmsPages;
//# sourceMappingURL=Pages.js.map