import { IPage } from '../index';
export default class CmsPages {
    pathProject: string;
    pathFiles: string;
    pathFull: string;
    options: {
        root: string;
    };
    constructor(pathProject: string, pathFiles: string, options?: {
        root: string;
    });
    readonly readPagesTree: any[];
    readonly readPagesList: IPage[];
    readonly readFilesFolder: string[];
}
