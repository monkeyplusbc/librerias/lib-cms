"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readDatas_1 = require("../utils/readDatas");
const shared_1 = require("../utils/shared");
class DataPage {
    constructor(pathProject, pathFiles) {
        this.pathProject = pathProject;
        this.pathFiles = pathFiles;
    }
    get readData() {
        const rPage = readDatas_1.readData(this.pathProject);
        const readFiles = shared_1.readDir(this.pathProject);
        return readDatas_1.readDatas(rPage)(readFiles)(readFiles(this.pathFiles), this.pathFiles)({});
    }
}
exports.default = DataPage;
//# sourceMappingURL=Data.js.map