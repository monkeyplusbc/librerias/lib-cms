export default class DataPage {
    pathProject: string;
    pathFiles: string;
    constructor(pathProject: string, pathFiles: string);
    readonly readData: any[];
}
